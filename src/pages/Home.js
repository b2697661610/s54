import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
			// title: "Error 404 - Page not found.",
			// content: "The page you are looking for connot be found.",
			// destination: "/courses",
			// label: "Enroll now" 
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere.",
		destination: "/courses",
		label: "Enroll now!"

	}

	return (
		<>
		< Banner data= {data} />
      	< Highlights/>
      	
		</>
	)
}
